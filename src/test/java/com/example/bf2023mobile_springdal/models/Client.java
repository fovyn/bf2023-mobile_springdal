package com.example.bf2023mobile_springdal.models;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Client {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;
    @Basic
    @Column(name = "firstname", nullable = true, length = 255)
    private String firstname;
    @Basic
    @Column(name = "lastname", nullable = true, length = 50)
    private String lastname;
    @Basic
    @Column(name = "createdat", nullable = true)
    private Date createdat;
    @OneToMany(mappedBy = "clientByClientId")
    private Collection<Emprunt> empruntsById;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id && Objects.equals(firstname, client.firstname) && Objects.equals(lastname, client.lastname) && Objects.equals(createdat, client.createdat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, createdat);
    }

    public Collection<Emprunt> getEmpruntsById() {
        return empruntsById;
    }

    public void setEmpruntsById(Collection<Emprunt> empruntsById) {
        this.empruntsById = empruntsById;
    }
}
