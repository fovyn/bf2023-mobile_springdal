package com.example.bf2023mobile_springdal.models;

import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Emprunt {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;
    @Basic
    @Column(name = "client_id", nullable = true)
    private Long clientId;
    @Basic
    @Column(name = "dateemprunt", nullable = true)
    private Timestamp dateemprunt;
    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client clientByClientId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Timestamp getDateemprunt() {
        return dateemprunt;
    }

    public void setDateemprunt(Timestamp dateemprunt) {
        this.dateemprunt = dateemprunt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Emprunt emprunt = (Emprunt) o;
        return id == emprunt.id && Objects.equals(clientId, emprunt.clientId) && Objects.equals(dateemprunt, emprunt.dateemprunt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, dateemprunt);
    }

    public Client getClientByClientId() {
        return clientByClientId;
    }

    public void setClientByClientId(Client clientByClientId) {
        this.clientByClientId = clientByClientId;
    }
}
