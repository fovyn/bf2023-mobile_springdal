package com.example.bf2023mobile_springdal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bf2023MobileSpringDalApplication {

    public static void main(String[] args) {
        SpringApplication.run(Bf2023MobileSpringDalApplication.class, args);
    }

}
