package com.example.bf2023mobile_springdal.pl.controllers;

import com.example.bf2023mobile_springdal.dal.entities.Livre;
import com.example.bf2023mobile_springdal.dal.repositories.LivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(path = {"/livres"})
public class LivreController {
    private final LivreRepository repository;

    @Autowired
    public LivreController(LivreRepository repository) {
        this.repository = repository;
    }


    @GetMapping(path = {"", "/"})
    public ResponseEntity<List<Livre>> getAllAction() {
//        List<Livre> livres = repository.

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(repository.findAllByActive(true));
    }

    @GetMapping(path = {"/{id:[0-9]+}"})
    public ResponseEntity<Livre> getOneAction(
            @PathVariable(name = "id") Livre book
    ) {
        return ResponseEntity.ok(book);
    }
}
