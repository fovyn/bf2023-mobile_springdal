package com.example.bf2023mobile_springdal.dal.repositories;

import com.example.bf2023mobile_springdal.dal.entities.Livre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivreRepository extends JpaRepository<Livre, Long> {
    @Query(value = "SELECT l FROM Livre l WHERE l.active = :active")
    List<Livre> findAllByActive(@Param("active") boolean active);

}
