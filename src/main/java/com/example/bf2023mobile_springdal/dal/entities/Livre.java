package com.example.bf2023mobile_springdal.dal.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity
@Table(name = "livre")
@Getter
@Setter
@EntityListeners(value = {AuditingEntityListener.class})
public class Livre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String titre;

    private boolean active = true;

    @CreatedDate
    private LocalDateTime createdAt;
}
